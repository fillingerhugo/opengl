#define GL_SILENCE_DEPRECATION
#include <GLFW/glfw3.h>
#include "openGlDisplay.hpp"
#include "point.hpp"
#include <vector>
#include "enveloppe.hpp"

int main(void)
{
    
    
    std::vector<Point> T;
    Polygone P;
    for (int i = 0; i < 10; i++) {
      int Px = rand() % 1000;
      int Py = rand() % 1000;
      Point pointUn = Point(Px, Py);
      T.push_back(pointUn);
    }
    enveloppe(T, P);
}
