#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include "point.hpp"
#include "polygone.hpp"

// trace un segment entre deux points a et b
void segment(const Point & a, const Point & b);

// trace le polygone P dans une fenÍtre graphique
void trace(const Polygone &P);

#endif
