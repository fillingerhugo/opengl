#ifndef ENVELOPPE_H
#define ENVELOPPE_H

#include "point.hpp"
#include "polygone.hpp"

#include <vector>

using namespace std;

// Construit dans le polygone P líenveloppe convexe des trois points a,b,c. On suppose P initialement vide.
// La fonction renvoie líadresse du sommet de coordonnÈes c.
Sommet* enveloppe(const Point &a, const Point &b, const Point &c, Polygone &P);

// Construit dans le polygone P líenveloppe convexe de l'ensemble de points donnÈ dans T.
// On suppose P initialement vide.
void enveloppe(vector<Point>&T, Polygone &P);

#endif
