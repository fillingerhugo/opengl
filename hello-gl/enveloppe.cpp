#include "enveloppe.hpp"
#include "point.hpp"
#include "polygone.hpp"
#include "openGlDisplay.hpp"
#include <vector>

using namespace std;


// Construit dans le polygone P l’enveloppe convexe des trois points a,b,c. On suppose P initialement vide.
// La fonction renvoie l’adresse du sommet de coordonnées c.
Sommet* enveloppe(const Point &a, const Point &b, const Point &c, Polygone &P)
{
    std::vector<Point> points = {a, b, c};
    std::sort(points.begin(),
              points.end(),
              [](Point& a, Point& b)
    {
        return a.x() < b.x();
    });
    
    if(0 < c.aGauche(a, b)){
        Sommet* nvxSommet = P.ajouteSommet(a);
        Sommet* sommet2 = P.ajouteSommet(b,nvxSommet);
        return P.ajouteSommet(c, sommet2);
    } else {
        Sommet* nvxSommet = P.ajouteSommet(a);
        Sommet* sommet2 = P.ajouteSommet(c, nvxSommet);
        P.ajouteSommet(b, sommet2);
        return sommet2;
    }
}


// Construit dans le polygone P l’enveloppe convexe de l'ensemble de points donné dans T.
// On suppose P initialement vide.
void enveloppe(vector<Point>&T, Polygone &P)
{
    int loop = 0;
    Sommet* sommetPrecedent = nullptr;
    Sommet* sommetSuivant = nullptr;
    Sommet* sommetPrecedentCheck = nullptr;
    Sommet* sommetSuivantCheck = nullptr;
    // Tri les points par leur coordonnées X
    std::sort(T.begin(),
              T.end(),
              [](Point& a, Point& b)
    {
        return a.x() < b.x();
    });
    
    // Loop pour chaque point du vector
    for(auto point : T){
        
        // Si c'est le premier ajout d'un point on défini la variable sommet précedent par lui même.
        if (loop < 3){
            sommetPrecedent = P.ajouteSommet(point, sommetPrecedent);
            loop++;
        } else {
            
            sommetSuivantCheck = sommetPrecedent->suivant();

            // Vérifi si la droite utilisant les deux derniers points précedant celui que l'on veut ajouter est à gauche ou droite du point
            int precedentAGauche = point.aGauche(sommetPrecedent->precedent()->coordonnees(), sommetPrecedent->coordonnees());
            
            // Vérifi si la droite utilisant le  dernier point ainsi que son suivant est à gauche ou droite du point que l'on veut ajouter
//            int suivantAGauche = point.aGauche(sommetPrecedent->coordonnees(), sommetPrecedent->suivant()->coordonnees());
            
            // Si le point précedent est à gauche
            if (0 > precedentAGauche) {
                
                // Sauvegarde le point précedent du sommet précedent
                sommetPrecedentCheck = sommetPrecedent->precedent();
                
                // Supprime le sommet précédent
                P.supprimeSommet(sommetPrecedent);
                // While qui vérifie si les segments précédent sont à garder, tant que le point est à droite du segment
                while (0 > point.aGauche(sommetPrecedent->coordonnees(), sommetPrecedent-> precedent()->coordonnees())) {
                    P.supprimeSommet(sommetPrecedent);
                }
                // ajout le sommet
                 P.ajouteSommet(point, sommetPrecedent);
            }
             // Vérifie si le point est a gauche des segments suivant
//            if (0 > suivantAGauche) {
//
//                P.supprimeSommet(sommetSuivantCheck);
//
//                while (0 > point.aGauche(sommetSuivantCheck->coordonnees(), sommetSuivantCheck->suivant()->coordonnees())) {
//                    sommetSuivantCheck = sommetSuivantCheck->suivant();
//                        P.supprimeSommet(sommetSuivant);
//                }
//            }
            else {
                P.ajouteSommet(point, sommetPrecedent);
            }
        }
    }
    display3(T,P);
}
