#include "affichage.hpp"
#include "point.hpp"
#include "polygone.hpp"
#include "openGlDisplay.hpp"
// trace un segment entre deux points a et b
void segment(const Point &a, const Point &b)
{
    //display(&a, &b);
}


// trace le polygone P dans une fenêtre graphique
void trace(const Polygone &P)
{
    Sommet* s = P.premier();
    do {
        segment(s->coordonnees(), s ->suivant()->coordonnees());
        s = s -> suivant();
    } while (s != P.premier());

}




