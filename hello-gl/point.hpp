#ifndef POINT_H
#define POINT_H

class Point
{

public:
    // constructeurs
    Point() : d_x{0},d_y{0} {}
    Point(float x, float y) : d_x{x},d_y{y} {}
    // accesseurs
    float x() const {return d_x;}
    float y() const {return d_y;}

    // renvoie 1, -1 ou 0 selon que le point auquel la mÈthode est appliquÈe est
    // ‡ gauche de, ‡ droite de, ou sur la droite (ab) orientÈe de a vers b.
    int aGauche(const Point &a, const Point &b) const;


private:
    // coordonnÈes
    float d_x, d_y;
};

#endif
