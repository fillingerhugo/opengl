#ifndef POLYGONE_H
#define POLYGONE_H

#include "point.hpp"

class Sommet
{
friend class Polygone;
public:
    // accesseurs
    Point coordonnees() const {return d_coordonnees;}
    Sommet* suivant() const {return d_suivant;}
    Sommet* precedent() const {return d_precedent;}

private:
    // constructeur ‡ partir d'un point p
    Sommet(const Point &p, Sommet *suivant=nullptr, Sommet *precedent=nullptr)
    : d_coordonnees{p}, d_suivant{suivant}, d_precedent{precedent} {}
    // destructeur vide privÈ
    ~Sommet() = default;

    // coordonnÈes
    Point d_coordonnees;
    // sommets suivant et precedent
    Sommet *d_suivant, *d_precedent;
};


class Polygone
{
public:
    // constructeur par dÈfaut
    Polygone() : d_premier{nullptr} {}
    // destructeur
    ~Polygone();
    // accesseurs
    Sommet* premier() const {return d_premier;}

    // Ajoute un nouveau sommet au polygone. Les coordonnÈes du sommet ‡ ajouter sont celles du point p.
    // sommetPrecedent est soit un pointeur sur líun des sommets dÈj‡ prÈsents dans le polygone,
    // soit un pointeur nul si le polygone est vide.
    // Dans le premier cas, le nouveau sommet devient le successeur du sommet díadresse sommetPrecedent.
    // Dans le deuxiËme cas, le nouveau sommet devient líunique sommet du polygone.
    // Dans tous les cas, la mÈthode renvoie líadresse du nouveau sommet.
    Sommet* ajouteSommet(const Point &p, Sommet *sommetPrecedent=nullptr);

    // Supprime le sommet díadresse s du polygone.
    // On suppose que s est effectivement prÈsent dans le polygone.
    void supprimeSommet(Sommet *s);


private:

    // un sommet quelconque du polygone. nullptr si le polygone est vide
    Sommet* d_premier;
};

#endif
