#ifndef openGlDisplay_hpp
#define openGlDisplay_hpp
#include "point.hpp"
#include "polygone.hpp"
#include <stdio.h>
#include <vector>

using namespace std;

int display(Point &a,Point &b);
int display2(Polygone &P);
int display3(vector<Point>&T, Polygone &P);
#endif /* openGlDisplay_hpp */
