#include "polygone.hpp"
#include "point.hpp"
#include <functional>  // std::invoke

using namespace std;

// Impl�mentation des m�thodes de la classe Polygone

// destructeur
Polygone::~Polygone()
{
}

// Ajoute un nouveau sommet au polygone. Les coordonn�es du sommet � ajouter sont celles du point p.
// sommetPrecedent est soit un pointeur sur l�un des sommets d�j� pr�sents dans le polygone,
// soit un pointeur nul si le polygone est vide.
// Dans le premier cas, le nouveau sommet devient le successeur du sommet d�adresse sommetPrecedent.
// Dans le deuxi�me cas, le nouveau sommet devient l�unique sommet du polygone.
// Dans tous les cas, la m�thode renvoie l�adresse du nouveau sommet.
Sommet* Polygone::ajouteSommet(const Point &p, Sommet* sommetPrecedent)
{
    Sommet* nvxSommet =new Sommet(p);
    if (premier() == nullptr){
       nvxSommet-> d_precedent =  nvxSommet;
       nvxSommet-> d_suivant =  nvxSommet;
        this-> d_premier = nvxSommet;
    } else {
        nvxSommet-> d_suivant = sommetPrecedent -> suivant();
        nvxSommet-> d_precedent = sommetPrecedent;
        sommetPrecedent -> d_suivant = nvxSommet;
        sommetPrecedent -> suivant()-> suivant()-> d_precedent = nvxSommet;
    }
    return nvxSommet;
}


// Supprime le sommet d�adresse s du polygone.
// On suppose que s est effectivement pr�sent dans le polygone.
void Polygone::supprimeSommet(Sommet* s)
{
    s -> precedent() -> d_suivant = s -> d_suivant;
    s -> suivant() -> d_precedent = s -> d_precedent;
}





    
