#define GL_SILENCE_DEPRECATION

#include "openGlDisplay.hpp"
#include <GLFW/glfw3.h>
#include "point.hpp"
#include "polygone.hpp"
int display(Point &a,Point &b)
{
    GLFWwindow* window;
     /* Initialize the library */
     if (!glfwInit())
      return -1;
    
    
     /* Create a windowed mode window and its OpenGL context */
     window = glfwCreateWindow(1000, 1000, "Hello World", NULL, NULL);
     if (!window)
     {
      glfwTerminate();
      return -1;
     }
     /* Make the window’s context current */
     glfwMakeContextCurrent(window);
     /* Loop until the user closes the window */
     while (!glfwWindowShouldClose(window))
     {
      glClearColor(1.f, 1.f, 1.f, 1.f);
      glClear(GL_COLOR_BUFFER_BIT);
      /* Render here */
      glMatrixMode( GL_MODELVIEW );
      glLoadIdentity();
      glColor3f(1.f, 0.f, 0.f);
      //glVertex2f(-0.5, -0.5);
       glBegin(GL_LINES);
       glVertex2f(a.x(), a.y());
       glVertex2f(b.x(), b.y());
       glEnd();
      /* Swap front and back buffers */
      glfwSwapBuffers(window);
      /* Poll for and process events */
      glfwPollEvents();
     }
     glfwTerminate();
     return 0;
}
int display2(Polygone &P)
{
    GLFWwindow* window;
     /* Initialize the library */
     if (!glfwInit())
      return -1;
     /* Create a windowed mode window and its OpenGL context */
     window = glfwCreateWindow(1000, 1000, "Hello World", NULL, NULL);
     if (!window)
     {
      glfwTerminate();
      return -1;
     }
     /* Make the window’s context current */
     glfwMakeContextCurrent(window);
     /* Loop until the user closes the window */
     while (!glfwWindowShouldClose(window))
     {
      glClearColor(1.f, 1.f, 1.f, 1.f);
      glClear(GL_COLOR_BUFFER_BIT);
      /* Render here */
      glMatrixMode( GL_MODELVIEW );
      glLoadIdentity();
      glColor3f(1.f, 0.f, 0.f);
      //glVertex2f(-0.5, -0.5);
       glBegin(GL_LINES);
         
         
         
       glEnd();
      /* Swap front and back buffers */
      glfwSwapBuffers(window);
      /* Poll for and process events */
      glfwPollEvents();
     }
     glfwTerminate();
     return 0;
}

int display3(vector<Point>&T, Polygone &P)
{
 GLFWwindow* window;
 /* Initialize the library */
 if (!glfwInit())
  return -1;
 /* Create a windowed mode window and its OpenGL context */
 window = glfwCreateWindow(1000, 1000, "Hello World", NULL, NULL);
 if (!window)
 {
  glfwTerminate();
  return -1;
 }
    int loop = 0;

  /* Make the window’s context current */
  glfwMakeContextCurrent(window);
  /* Loop until the user closes the window */
 while (!glfwWindowShouldClose(window))
 {
   glClearColor (0, 0, 0, 0);
   glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(0,1024,1024,0,1024,-1024);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glPointSize(4);
   glBegin(GL_POINTS);
   glColor4f(1,1,1,1);
   for (auto point: T) {
     glVertex2f(point.x(), point.y());
   }
   glEnd();
   glBegin(GL_LINES);
     
     Sommet* s = P.premier()->suivant();
     for (int i = 0; i< 10; i++) {
          glVertex2f(s->coordonnees().x(), s->coordonnees().y());
          glVertex2f(s->suivant()->coordonnees().x(), s->suivant()->coordonnees().y());
          glVertex2f(s->suivant()->coordonnees().x(), s->suivant()->coordonnees().y());
          glVertex2f(s->suivant()->suivant()->coordonnees().x(), s->suivant()->suivant()->coordonnees().y());
          glVertex2f(s->suivant()->suivant()->coordonnees().x(), s->suivant()->suivant()->coordonnees().y());
          glVertex2f(s->coordonnees().x(), s->coordonnees().y());
          s = s->suivant();
        }

//   int highNum;
//   int i;
//   for (i = 0; i < T.size(); i++) {
//     if(T[i].x() < T[i + 1].x()) highNum = T[i].x();
//     glVertex2f(T[i].x(), T[i].y());
//   }
     
   glEnd();
   /* Swap front and back buffers */
   glfwSwapBuffers(window);
   /* Poll for and process events */
   glfwPollEvents();
 }
 glfwTerminate();
 return 0;
}

