#include "point.hpp"

// renvoie 1, -1 ou 0 selon que le point auquel la mÈthode est appliquÈe est
// ‡ gauche de, ‡ droite de, ou sur la droite (ab) orientÈe de a vers b.
int Point::aGauche(const Point &a, const Point &b) const
{
    int Result = (b.x() - a.x())*(this->y() - a.y()) - (this->x() - a.x())*(b.y() - a.y());
    if (Result > 0) {
        return 1;
    } else if (Result == 0){
        return 0;
    } else {
        return -1;
    }
}
